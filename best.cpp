#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>

#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP 			make_pair
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<62

typedef long long ll;

using namespace std;
ll F(ll l  , ll h , vector<ll>vec){
    sort(vec.begin()+l , vec.begin()+h);
    return vec[(h+l)/2];
}
ll FF(ll l , ll h , vector<ll> vec){
    vector<ll>mm;
    ll n = h-l+1;
    mm.assign((n+4)/5 , 0);
    ll i=0;
    for(i; i <(n+4)/5 ; i++) mm[i]=F(l+i*5 , min (h , l+i*5+5-1) ,vec);
    if(i== 1) return mm[0];
    else return FF(0 , i-1 , mm);
}
vector<ll>vec;

ll part(ll l , ll h , ll med){
    ll nn ;
    For(i , l ,h+1) if(med== vec[i]) {nn=i ;break;}
    swap(vec[nn] , vec[h]);
    ll i = l-1;
    For(j ,l , h) if(vec[j] <=med) swap(vec[++i] , vec[j]);
    swap(vec[++i], vec[h]);
    return i;
}
ll QS(ll l , ll h ,ll k){
    ll n = h-l+1;
    ll med = FF(l , h , vec);
    ll pr = part(l , h , med);
    if(pr == l+k-1) return vec[pr];
    else if(pr < l+k-1) return QS(pr+1, h  , k-pr+l-1);
    else QS(l , pr-1 , k);
}
int main(){
    //Test;
    ll t; cin >> t;
    while(t--){
        ll n ; cin >>n;
        vec.clear();
        Rep(i , n)
            { ll x ;cin >>x; vec.push_back(x);}
        ll k ;cin >>k;
        cout<<QS(0 , n-1 ,k)<<endl;
    }
}
